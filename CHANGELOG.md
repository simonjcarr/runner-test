# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.81](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.80...v0.1.81) (2021-05-11)

### [0.1.80](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.79...v0.1.80) (2021-05-11)

### [0.1.79](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.78...v0.1.79) (2021-05-11)

### [0.1.78](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.77...v0.1.78) (2021-05-11)

### [0.1.77](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.76...v0.1.77) (2021-05-11)

### [0.1.76](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.75...v0.1.76) (2021-05-11)

### [0.1.75](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.74...v0.1.75) (2021-05-11)

### [0.1.74](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.73...v0.1.74) (2021-05-11)

### [0.1.73](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.72...v0.1.73) (2021-05-11)

### [0.1.72](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.71...v0.1.72) (2021-05-11)

### [0.1.71](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.70...v0.1.71) (2021-05-11)

### [0.1.70](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.69...v0.1.70) (2021-05-11)

### [0.1.69](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.68...v0.1.69) (2021-05-11)

### [0.1.68](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.67...v0.1.68) (2021-05-11)

### [0.1.67](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.66...v0.1.67) (2021-05-11)

### [0.1.66](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.65...v0.1.66) (2021-05-11)

### [0.1.65](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.64...v0.1.65) (2021-05-11)

### [0.1.64](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.63...v0.1.64) (2021-05-11)

### [0.1.63](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.62...v0.1.63) (2021-05-11)

### [0.1.62](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.61...v0.1.62) (2021-05-11)

### [0.1.61](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.60...v0.1.61) (2021-05-11)

### [0.1.60](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.59...v0.1.60) (2021-05-11)

### [0.1.59](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.58...v0.1.59) (2021-05-11)

### [0.1.58](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.57...v0.1.58) (2021-05-11)

### [0.1.57](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.56...v0.1.57) (2021-05-10)

### [0.1.56](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.55...v0.1.56) (2021-05-10)

### [0.1.55](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.54...v0.1.55) (2021-05-10)

### [0.1.54](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.53...v0.1.54) (2021-05-10)

### [0.1.53](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.52...v0.1.53) (2021-05-10)

### [0.1.52](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.51...v0.1.52) (2021-05-10)

### [0.1.51](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.50...v0.1.51) (2021-05-10)

### [0.1.50](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.49...v0.1.50) (2021-05-10)

### [0.1.49](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.48...v0.1.49) (2021-05-10)

### [0.1.48](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.47...v0.1.48) (2021-05-10)

### [0.1.47](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.46...v0.1.47) (2021-05-10)

### [0.1.46](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.45...v0.1.46) (2021-05-09)

### [0.1.45](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.44...v0.1.45) (2021-05-09)


### Bug Fixes

* bug in ci file ([e367486](https://gitlab.com/simonjcarr/runner-test/commit/e3674862285bd1934c8d6df6450a12aea4c10ce1))

### [0.1.44](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.43...v0.1.44) (2021-05-09)

### [0.1.43](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.42...v0.1.43) (2021-05-09)


### Bug Fixes

* bug in ci file ([538f0b9](https://gitlab.com/simonjcarr/runner-test/commit/538f0b9850d31dedb3bbafeda5856c4654dbdca4))

### [0.1.42](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.41...v0.1.42) (2021-05-09)

### [0.1.41](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.40...v0.1.41) (2021-05-09)

### [0.1.40](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.39...v0.1.40) (2021-05-09)

### [0.1.39](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.38...v0.1.39) (2021-05-09)

### [0.1.38](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.37...v0.1.38) (2021-05-09)

### [0.1.37](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.36...v0.1.37) (2021-05-09)

### [0.1.36](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.35...v0.1.36) (2021-05-09)

### [0.1.35](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.34...v0.1.35) (2021-05-09)

### [0.1.34](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.33...v0.1.34) (2021-05-09)

### [0.1.33](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.32...v0.1.33) (2021-05-09)

### [0.1.32](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.31...v0.1.32) (2021-05-09)

### [0.1.31](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.30...v0.1.31) (2021-05-09)


### Features

* added deploy stage and updated deployment.yaml ([300b104](https://gitlab.com/simonjcarr/runner-test/commit/300b10406ffbb5ca02deadfb18e6e480eea5b400))

### [0.1.30](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.29...v0.1.30) (2021-05-09)

### [0.1.29](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.28...v0.1.29) (2021-05-09)


### Bug Fixes

* fix error in nginx.conf ([eb149fb](https://gitlab.com/simonjcarr/runner-test/commit/eb149fb623292ee02eff825176974a5baeac84bd))

### [0.1.28](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.27...v0.1.28) (2021-05-09)

### [0.1.27](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.26...v0.1.27) (2021-05-09)


### Bug Fixes

* fix error in location section of nginx.conf ([c073aec](https://gitlab.com/simonjcarr/runner-test/commit/c073aecb99879abfd40f8a1342fabbdaedacbc3e))

### [0.1.26](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.25...v0.1.26) (2021-05-09)

### [0.1.25](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.24...v0.1.25) (2021-05-09)


### Bug Fixes

* Fixed error in nginx.conf ([7fdeac0](https://gitlab.com/simonjcarr/runner-test/commit/7fdeac0f085902699548295ccd937e4c99509721))

### [0.1.24](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.23...v0.1.24) (2021-05-09)

### [0.1.23](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.22...v0.1.23) (2021-05-09)

### [0.1.22](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.21...v0.1.22) (2021-05-09)

### [0.1.21](https://gitlab.com/simonjcarr/runner-test/compare/v0.1.20...v0.1.21) (2021-05-09)

### [0.1.20](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.19...v0.1.20) (2021-05-09)

### [0.1.19](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.18...v0.1.19) (2021-05-09)

### [0.1.18](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.17...v0.1.18) (2021-05-09)

### [0.1.17](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.16...v0.1.17) (2021-05-09)

### [0.1.16](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.15...v0.1.16) (2021-05-09)


### Bug Fixes

* error in .gitlab-ci.yml file ([4fdeefb](https://gitlab.lab.simoncarr.co.uk///commit/4fdeefbcd440d6117d17dee209116f230dc8fa3b))

### [0.1.15](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.14...v0.1.15) (2021-05-09)

### [0.1.14](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.13...v0.1.14) (2021-05-09)

### [0.1.13](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.12...v0.1.13) (2021-05-09)

### [0.1.12](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.11...v0.1.12) (2021-05-09)


### Bug Fixes

* error in .gitlab-ci.yml file ([b99478e](https://gitlab.lab.simoncarr.co.uk///commit/b99478e70391cbd57e327529d34cbc6020209c63))

### [0.1.11](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.10...v0.1.11) (2021-05-09)

### [0.1.10](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.9...v0.1.10) (2021-05-09)

### [0.1.9](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.8...v0.1.9) (2021-05-09)

### [0.1.8](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.7...v0.1.8) (2021-05-09)


### Bug Fixes

* Fixed file name on gitlab-ci.yml to .gitlab-ci.yml ([660dc7b](https://gitlab.lab.simoncarr.co.uk///commit/660dc7b57dfbd31dad5c6c32af5cdd94db711e01))

### [0.1.7](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.6...v0.1.7) (2021-05-09)

### [0.1.6](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.5...v0.1.6) (2021-05-09)

### [0.1.5](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.4...v0.1.5) (2021-05-09)

### [0.1.4](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.4-alpha.1...v0.1.4) (2021-05-09)

### [0.1.4-alpha.1](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.4-alpha.0...v0.1.4-alpha.1) (2021-05-09)


### Features

* added line 6 to README.md ([03a12a5](https://gitlab.lab.simoncarr.co.uk///commit/03a12a5c93fa6702e690b462d4985d19f61696da))

### [0.1.4-alpha.0](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.3...v0.1.4-alpha.0) (2021-05-09)

### [0.1.3](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.3-alpha.0...v0.1.3) (2021-05-09)

### [0.1.3-alpha.0](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.2...v0.1.3-alpha.0) (2021-05-09)

### [0.1.2](https://gitlab.lab.simoncarr.co.uk///compare/v0.1.1...v0.1.2) (2021-05-09)

### [0.1.1](https://gitlab.simoncarr.co.uk///compare/v0.1.0...v0.1.1) (2021-05-09)

## 0.1.0 (2021-05-09)
